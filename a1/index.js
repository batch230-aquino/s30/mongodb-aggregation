db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$count: "onSale"}
    ]
);

db.fruits.aggregate(
    [
        {$match: {
            stock:{$gte:20}
        }},
        {$count: "stock"}
    ]
);

db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$group:
            {_id: "$supplier_id",fruit_onsale:{$avg: "$price"}}
        }
    ]
);

db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$group:
            {_id: "$supplier_id", highest_price: {$max: "price"}}
        }
    ]
);


db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$group:
            {_id: "$supplier_id", highest_price: {$max: "$price"}}
        }
    ]
);

db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$group:
            {_id: "$supplier_id", highest_price: {$min: "$price"}}
        }
    ]
);