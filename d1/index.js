db.fruits.insertMany([
    {
        name : "Apple",
        color : "Red",
        stock : 20,
        price: 40,
        supplier_id : 1,
        onSale : true,
        origin: [ "Philippines", "US" ]
    },

    {
        name : "Banana",
        color : "Yellow",
        stock : 15,
        price: 20,
        supplier_id : 2,
        onSale : true,
        origin: [ "Philippines", "Ecuador" ]
    },

    {
        name : "Kiwi",
        color : "Green",
        stock : 25,
        price: 50,
        supplier_id : 1,
        onSale : true,
        origin: [ "US", "China" ]
    },

    {
        name : "Mango",
        color : "Yellow",
        stock : 10,
        price: 120,
        supplier_id : 2,
        onSale : false,
        origin: [ "Philippines", "India" ]
    }
]);
//[SECTION] MongoDB Aggregation
/*
    -Used to generate,manipulate, and perform operations to create filtered
    results that can helps us to analyze the data.
*/

    //Using the aggregate method:
    /*
        -The "$match" is used to passed the document that meet the specified
        conditions condition(s) to the next stage/aggregation process.
        - Syntax:
                {$match{field: value}}
        -the "$group" is used to group elements together and field-value pairs of the data from the grouped elements.
        -Syntax:
            {$group:{_id: "value", fieldResult: "valueResult"}}
        -Using both $match and $group along with aggregation will find for products are on sale and will group the total amount fo stock for all 
        suppliers found.
        -the "$" symbol will refer to a field name that is available in the documents that are being aggregate.
    */

        db.fruits.aggregate(
            [
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
        ]
    );

    //Field projection with aggregation

    db.fruits.aggregate(
        [
        {$match: {onSale: true}},
        {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
        {$project: {_id: 0}}
    ]
    );

    //Sorting aggregated results
    /*
        -The "$sort" can be used to change the order of the new aggreagated result
        -syntax:
            {sort: {field: 1/-1}}
            1 > means lowest to highest
            -1 > means highest to lowest
    */

            db.fruits.aggregate(
                [
                {$match: {onSale: true}},
                {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
                {$sort: {total: 1}}
            ]
        );

        db.fruits.aggregate(
            [
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
            {$sort: {total: -1}}
        ]
    );

    //Aggregating result based on array fields
    /*
        -The "$unwind" deconstructs an array field from the collection/field with an array value to output a result
        -syntax:
            {$unwind: field}
    */

            db.fruits.aggregate(
                [
                    {$unwind: "$origin"},
                    {$group: 
                        {_id: $origin, fruits:{$sum: 1}
                    }
                }
           ]
        );

    //[SECTION] Other aggregate stages
    db.fruits.aggregate(
        [
            //count all yellow fruits

            {$match: {color: "Yellow"}},
            {$count: "Yellow Fruits"}
        ]
    );

    // $avg
    //get the average value of stock
    db.fruits.aggregate(
        [
            {$match: {color: "Yellow"}},
            {$group: 
                {_id: "$color", yellow_fruits_stock:
                {$avg: "$stock"}}
        }
        ]
    );

    //#min & #max

    db.fruits.aggregate(
        [
            {$match: {color: "Yellow"}},
            {$group: {_id: "color", yellow_fruits_stock: {$min: $stock}}}
        ]
    );
